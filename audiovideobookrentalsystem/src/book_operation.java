import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
public class book_operation extends javax.swing.JFrame {
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
    public book_operation() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(700,500);
        setLocation(400,50);
        updatejtable();
 }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/    
    public void close(){
   WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
   Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);

}
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/    
public void updatejtable(){
try{
String sql="select * from book";
pst=conn.prepareStatement(sql);
rs=pst.executeQuery();
txt_jtfbook.setModel(DbUtils.resultSetToTableModel(rs));


}
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
}   
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_addnb = new javax.swing.JButton();
        txt_updateb = new javax.swing.JButton();
        txt_deleteeb = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jtfbook = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_title = new javax.swing.JTextField();
        txt_autor = new javax.swing.JTextField();
        txt_amount = new javax.swing.JTextField();
        txt_home = new javax.swing.JButton();
        txt_search = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_addnb.setText("addnb");
        txt_addnb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_addnbActionPerformed(evt);
            }
        });

        txt_updateb.setText("updateb");
        txt_updateb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_updatebActionPerformed(evt);
            }
        });

        txt_deleteeb.setText("deleteeb");
        txt_deleteeb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_deleteebActionPerformed(evt);
            }
        });

        txt_jtfbook.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        txt_jtfbook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_jtfbookMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(txt_jtfbook);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("title");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("autor");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("amount");

        txt_home.setText("home");
        txt_home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_homeActionPerformed(evt);
            }
        });

        txt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_searchKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("search");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(25, 25, 25)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_deleteeb)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txt_updateb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_addnb, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 109, Short.MAX_VALUE)
                                        .addComponent(txt_home))))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_autor, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(48, 48, 48))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 365, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(119, 119, 119))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_addnb)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_updateb)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_deleteeb))
                            .addComponent(txt_home))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_autor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(48, 48, 48)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(136, 136, 136))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_jtfbookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_jtfbookMouseClicked
        try{
            int row=txt_jtfbook.getSelectedRow();
            String table_click=(txt_jtfbook.getModel().getValueAt(row,2).toString());
            String sql="select * from book where amount="+table_click+"";
            pst=conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if(rs.next())
            {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("author");
            txt_autor.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
            }
            
        }
        catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_txt_jtfbookMouseClicked
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_homeActionPerformed
       dispose();
       new HOME().setVisible(true);
    }//GEN-LAST:event_txt_homeActionPerformed

    private void txt_addnbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_addnbActionPerformed
         try{
             String sql="insert into book(title,author,amount)values(?,?,?) ";  
             pst=conn.prepareStatement(sql);
             pst.setString(1,txt_title.getText());
             pst.setString(2,txt_autor.getText());
             pst.setString(3,txt_amount.getText());
             pst.execute();
      
            JOptionPane.showMessageDialog(null,"registered sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
        updatejtable(); 
    }//GEN-LAST:event_txt_addnbActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_updatebActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_updatebActionPerformed
        try{
      String sql="update book set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_amount.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
        updatejtable();
    }//GEN-LAST:event_txt_updatebActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_deleteebActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_deleteebActionPerformed
   int p=JOptionPane.showConfirmDialog(null,"Do you really want to delete?","Delete",JOptionPane.YES_NO_OPTION);
    if(p==0){  
     try{
      String sql="delete from book where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"deleted sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
       updatejtable();
    }
    }//GEN-LAST:event_txt_deleteebActionPerformed

    private void txt_searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_searchKeyReleased
      try{
        String s="select * from book where title=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_search.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("author");
            txt_autor.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
        }
         
       }
       catch(Exception e){
       JOptionPane.showMessageDialog(null,e);
       }
    }//GEN-LAST:event_txt_searchKeyReleased

   /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/ 
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new book_operation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton txt_addnb;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_autor;
    private javax.swing.JButton txt_deleteeb;
    private javax.swing.JButton txt_home;
    private javax.swing.JTable txt_jtfbook;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_title;
    private javax.swing.JButton txt_updateb;
    // End of variables declaration//GEN-END:variables
}
