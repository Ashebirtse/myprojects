import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
import sun.audio.*;
import sun.audio.AudioStream;
public class customer_operation extends javax.swing.JFrame {
    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;
    ImageIcon format=null;
    String  filename=null;
    int s=0;
    byte[] person_image=null;
    private String Gender=null;
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public void defaultpicturedisplyed(){
    try{
           String sql="select * from ctable where id=1";
           pst=conn.prepareStatement(sql);
           rs=pst.executeQuery();
           if(rs.next()){
           byte[] imagedata=rs.getBytes("image");
           format=new ImageIcon(imagedata);
           image.setIcon(format);
           }
        }catch(Exception e){
        e.printStackTrace();
        }
    }
    public customer_operation() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(800,600);
        setLocation(50,50);
        updatejtable();
        defaultpicturedisplyed();
    }/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public void close1(){
       WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
       Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);
     }
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
public void updatejtable(){
try{
String sql="select * from ctable";
pst=conn.prepareStatement(sql);
rs=pst.executeQuery();
txt_jtfcustomer.setModel(DbUtils.resultSetToTableModel(rs));


}
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
}  
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable2 = new javax.swing.JTable();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        txt_deletecustomer = new javax.swing.JButton();
        txt_customerreg = new javax.swing.JButton();
        txt_home = new javax.swing.JButton();
        txt_id = new javax.swing.JTextField();
        txt_fn = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_sin = new javax.swing.JTextField();
        txt_ln = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        txt_jtfcustomer = new javax.swing.JTable();
        txt1_image = new javax.swing.JDesktopPane();
        image = new javax.swing.JLabel();
        txt_image = new javax.swing.JButton();
        txt_attach = new javax.swing.JButton();
        txt_path = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        sound = new javax.swing.JButton();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jTable2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(jTable2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(51, 255, 204));

        txt_deletecustomer.setBackground(new java.awt.Color(51, 204, 255));
        txt_deletecustomer.setText("deletecustomer");
        txt_deletecustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_deletecustomerActionPerformed(evt);
            }
        });

        txt_customerreg.setBackground(new java.awt.Color(51, 204, 255));
        txt_customerreg.setText("customerreg");
        txt_customerreg.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customerregActionPerformed(evt);
            }
        });

        txt_home.setBackground(new java.awt.Color(51, 153, 255));
        txt_home.setText("home");
        txt_home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_homeActionPerformed(evt);
            }
        });

        txt_id.setBackground(new java.awt.Color(0, 255, 51));
        txt_id.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_idActionPerformed(evt);
            }
        });
        txt_id.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_idKeyTyped(evt);
            }
        });

        txt_fn.setBackground(new java.awt.Color(0, 255, 51));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("id");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("sin");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("fn");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("ln");

        txt_sin.setBackground(new java.awt.Color(0, 255, 51));
        txt_sin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sinActionPerformed(evt);
            }
        });

        txt_ln.setBackground(new java.awt.Color(0, 255, 51));
        txt_ln.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_lnActionPerformed(evt);
            }
        });

        txt_jtfcustomer.setBackground(new java.awt.Color(51, 255, 255));
        txt_jtfcustomer.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        txt_jtfcustomer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_jtfcustomerMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(txt_jtfcustomer);

        image.setBackground(new java.awt.Color(51, 255, 51));

        txt1_image.setLayer(image, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout txt1_imageLayout = new javax.swing.GroupLayout(txt1_image);
        txt1_image.setLayout(txt1_imageLayout);
        txt1_imageLayout.setHorizontalGroup(
            txt1_imageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, txt1_imageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(image, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                .addContainerGap())
        );
        txt1_imageLayout.setVerticalGroup(
            txt1_imageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, txt1_imageLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(image, javax.swing.GroupLayout.DEFAULT_SIZE, 102, Short.MAX_VALUE)
                .addContainerGap())
        );

        txt_image.setBackground(new java.awt.Color(51, 153, 255));
        txt_image.setText("image");
        txt_image.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_imageActionPerformed(evt);
            }
        });

        txt_attach.setBackground(new java.awt.Color(51, 153, 255));
        txt_attach.setText("attachimage");
        txt_attach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_attachActionPerformed(evt);
            }
        });

        txt_path.setBackground(new java.awt.Color(0, 255, 51));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("gender");

        buttonGroup1.add(jRadioButton1);
        jRadioButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jRadioButton1.setText("Male");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        buttonGroup1.add(jRadioButton2);
        jRadioButton2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jRadioButton2.setText("Female");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        sound.setText("sound");
        sound.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                soundActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_home)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_deletecustomer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_customerreg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(5, 5, 5)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_sin, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING))
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txt_fn, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE)
                                            .addComponent(txt_ln))))
                                .addGap(14, 14, 14)))
                        .addGap(18, 18, 18)
                        .addComponent(txt_image, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 669, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txt_path, javax.swing.GroupLayout.PREFERRED_SIZE, 302, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(37, 37, 37))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(369, 369, 369)
                                        .addComponent(txt_attach, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jRadioButton1)
                                                .addGap(18, 18, 18)
                                                .addComponent(jRadioButton2))
                                            .addComponent(sound, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(51, 51, 51)))
                        .addComponent(txt1_image, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(216, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(txt_deletecustomer))
                                    .addComponent(txt_home))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_customerreg))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addComponent(txt_image, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_sin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txt_fn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txt_ln, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jRadioButton1)
                            .addComponent(jRadioButton2))
                        .addGap(5, 5, 5)
                        .addComponent(txt_attach, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_path, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sound, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(txt1_image, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(131, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_homeActionPerformed
        dispose();
        new HOME().setVisible(true);
       
    }//GEN-LAST:event_txt_homeActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_sinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sinActionPerformed

    private void txt_lnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_lnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_lnActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_customerregActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customerregActionPerformed
    try{
        String sql="insert into ctable(id,sin,fn,ln,gender,person_image)values(?,?,?,?,?,?) ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_id.getText());
        pst.setString(2,txt_sin.getText());
        pst.setString(3,txt_fn.getText());
        pst.setString(4,txt_ln.getText());
        pst.setString(5,Gender);
        pst.setBytes(6,person_image);
        pst.execute();
      
        JOptionPane.showMessageDialog(null,"registered sucessfully");
      
         
      }
      catch(Exception e)
       {
         JOptionPane.showMessageDialog(null,e);
       
       }
    updatejtable();
    }//GEN-LAST:event_txt_customerregActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_deletecustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_deletecustomerActionPerformed
        int p=JOptionPane.showConfirmDialog(null,"Do you really want to delete?","Delete",JOptionPane.YES_NO_OPTION);
        if(p==0){
        try{
        String s="select * from ctable where id=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_id.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
        
        try{
           String sql="delete from ctable where id=? ";  
           pst=conn.prepareStatement(sql);
           pst.setString(1,txt_id.getText());
           pst.execute();
      
           JOptionPane.showMessageDialog(null,"deleted sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
        }
        else{
           JOptionPane.showMessageDialog(null,"there is no customer with this id");
        }
        }
       catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }
      updatejtable();
        }
    }//GEN-LAST:event_txt_deletecustomerActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_jtfcustomerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_jtfcustomerMouseClicked
        try{
            int row=txt_jtfcustomer.getSelectedRow();
            String table_click=(txt_jtfcustomer.getModel().getValueAt(row,0).toString());
            String sql="select * from ctable where id="+table_click+"";
            pst=conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if(rs.next())
            {
            String add1=rs.getString("id");
            txt_id.setText(add1);
            String add2=rs.getString("sin");
            txt_sin.setText(add2);
            String add3=rs.getString("fn");
            txt_fn.setText(add3);
            String add4=rs.getString("ln");
            txt_ln.setText(add4);
            byte[] imagedata=rs.getBytes("person_image");
           format=new ImageIcon(imagedata);
           image.setIcon(format);
            
            }
            
        }
        catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }
        try{
           String sql="select * from ctable where id=?";
           pst=conn.prepareStatement(sql);
           pst.setString(1,txt_id.getText());
           rs=pst.executeQuery();
           if(rs.next()){
           byte[] imagedata=rs.getBytes("image");
           format=new ImageIcon(imagedata);
           image.setIcon(format);
           }
        }catch(Exception e){
        e.printStackTrace();
        }
    }//GEN-LAST:event_txt_jtfcustomerMouseClicked

    private void txt_imageActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_imageActionPerformed
        try{
           String sql="select person_image from ctable where id=?";
           pst=conn.prepareStatement(sql);
           pst.setString(1,txt_id.getText());
           rs=pst.executeQuery();
           if(rs.next()){
           byte[] imagedata=rs.getBytes("person_image");
           format=new ImageIcon(imagedata);
           image.setIcon(format);
           }
        }catch(Exception e){
        e.printStackTrace();
        }
    }//GEN-LAST:event_txt_imageActionPerformed

    private void txt_attachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_attachActionPerformed
       JFileChooser chooser=new JFileChooser();
       chooser.showOpenDialog(null);
       File f=chooser.getSelectedFile();
       filename =f.getAbsolutePath();
       txt_path.setText(filename);
        try{
            File image=new File (filename);
            FileInputStream fis=new FileInputStream(image);
            ByteArrayOutputStream bos=new ByteArrayOutputStream();
            byte[] buf=new byte[1024];
            for(int readNum;(readNum=fis.read(buf))!=-1;){
            bos.write(buf,0,readNum);
            }
            person_image=bos.toByteArray();
       }catch(Exception e){
       
       }
    }//GEN-LAST:event_txt_attachActionPerformed

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
       Gender="M";
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
       Gender="F";
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void soundActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_soundActionPerformed
        InputStream in;
        try{
        in=new FileInputStream(new File("C:\\Users\\ashebir\\Desktop\\WIN_20170308_233712.MP4"));
        AudioStream as=new AudioStream(in);
        AudioPlayer.player.start(as);
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_soundActionPerformed

    private void txt_idKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_idKeyTyped
        char c=evt.getKeyChar();
        if(!(Character.isDigit(c) || (c==KeyEvent.VK_BACK_SPACE) || (c==KeyEvent.VK_DELETE)))
            evt.consume();
    }//GEN-LAST:event_txt_idKeyTyped

    private void txt_idActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_idActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_idActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new customer_operation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JLabel image;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JRadioButton jRadioButton2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable2;
    private javax.swing.JButton sound;
    private javax.swing.JDesktopPane txt1_image;
    private javax.swing.JButton txt_attach;
    private javax.swing.JButton txt_customerreg;
    private javax.swing.JButton txt_deletecustomer;
    private javax.swing.JTextField txt_fn;
    private javax.swing.JButton txt_home;
    private javax.swing.JTextField txt_id;
    private javax.swing.JButton txt_image;
    private javax.swing.JTable txt_jtfcustomer;
    private javax.swing.JTextField txt_ln;
    private javax.swing.JTextField txt_path;
    private javax.swing.JTextField txt_sin;
    // End of variables declaration//GEN-END:variables

}
