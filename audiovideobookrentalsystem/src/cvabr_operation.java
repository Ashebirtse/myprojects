import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
public class cvabr_operation extends javax.swing.JFrame {
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public cvabr_operation() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(1000,500);
        setLocation(300,50);
    }
 /*///////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public void updatejtable(){
   try{
       String sql="select * from car";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfcvabr.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     }
     }  
      public void updatejtable1(){
   try{
       String sql="select * from cvr";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfcvabr.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     }
     }  
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
   public void close(){
       WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
       Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);
   }
     /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_urnv = new javax.swing.JButton();
        txt_urtv = new javax.swing.JButton();
        txt_urna = new javax.swing.JButton();
        txt_urta = new javax.swing.JButton();
        txt_urnb = new javax.swing.JButton();
        txt_urtb = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_id = new javax.swing.JTextField();
        txt_title = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jtfcvabr = new javax.swing.JTable();
        txt_rv = new javax.swing.JButton();
        txt_ra = new javax.swing.JButton();
        txt_rb = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        txt_quantity = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_urnv.setText("update for rented video");
        txt_urnv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urnvActionPerformed(evt);
            }
        });

        txt_urtv.setText("update for returned video");
        txt_urtv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urtvActionPerformed(evt);
            }
        });

        txt_urna.setText("update for rented audio");
        txt_urna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urnaActionPerformed(evt);
            }
        });

        txt_urta.setText("update for returned audio");
        txt_urta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urtaActionPerformed(evt);
            }
        });

        txt_urnb.setText("update for rented book");
        txt_urnb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urnbActionPerformed(evt);
            }
        });

        txt_urtb.setText("update for returned book");
        txt_urtb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_urtbActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("id");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("title");

        jButton7.setText("home");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        txt_jtfcvabr.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(txt_jtfcvabr);

        txt_rv.setText("show rented video");
        txt_rv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rvActionPerformed(evt);
            }
        });

        txt_ra.setText("show rented audio");
        txt_ra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_raActionPerformed(evt);
            }
        });

        txt_rb.setText("show rented book");
        txt_rb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rbActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("quantity");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton7)
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_urnv, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_urtv, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_urna, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_urta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_urnb, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_urtb, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(txt_ra, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                                .addComponent(txt_rv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(txt_rb, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_id, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_title, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_quantity, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 362, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(245, 245, 245))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton7)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txt_urnv)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_urtv))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(txt_rv)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(txt_urna, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_urta)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(txt_urnb)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_urtb))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(46, 46, 46)
                                .addComponent(txt_rb)))
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(txt_ra)))
                .addGap(0, 195, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
       dispose();
       new HOME().setVisible(true);
        
    }//GEN-LAST:event_jButton7ActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urnvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urnvActionPerformed
    int cheker=0,cheker1=0;
    try{
      String sql1="select * from  vtable where title=?  ";  
      pst=conn.prepareStatement(sql1);
      pst.setString(1,txt_title.getText());
      pst.execute();
      rs=pst.executeQuery();
      String a=rs.getString("amount");
      cheker=Integer.parseInt(a);
      cheker1=Integer.parseInt(txt_quantity.getText());
      
     }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
       }
     if(cheker>=cheker1){
     try{
      String sql="update vtable set amount=amount-? where title=?  ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
        String sql="insert into cvr(id,title,quantity)values(?,?,?) ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_id.getText());
        pst.setString(2,txt_title.getText());
        pst.setString(3,txt_quantity.getText());
        pst.execute();
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
       
     }
     else
       JOptionPane.showMessageDialog(null,"not enough videos available");  
     
    }//GEN-LAST:event_txt_urnvActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urtvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urtvActionPerformed
    try{
      String sql="update vtable set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
      String sql="delete from cvr where id=? and title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_id.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_urtvActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urnaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urnaActionPerformed
    try{
      String sql="update autable set amount=amount-? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
        String sql="insert into car(id,title,quantity)values(?,?,?) ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_id.getText());
        pst.setString(2,txt_title.getText());
        pst.setString(3,txt_quantity.getText());
      
        pst.execute();
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_urnaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urtaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urtaActionPerformed
    try{
      String sql="update autable set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
      String sql="delete from car where id=? and title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_id.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_urtaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urtbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urtbActionPerformed
    try{
      String sql="update book set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
      String sql="delete from cbr where id=? and title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_id.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_urtbActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_urnbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_urnbActionPerformed
     try{
      String sql="update book set amount=amount-? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_quantity.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();     
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       }
       try{
        String sql="insert into cbr(id,title,quantity)values(?,?,?) ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_id.getText());
        pst.setString(2,txt_title.getText());
        pst.setString(3,txt_quantity.getText());
        pst.execute();
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_urnbActionPerformed

    private void txt_rvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rvActionPerformed
          
   try{
       String sql="select * from cvr";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfcvabr.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     }
      
    }//GEN-LAST:event_txt_rvActionPerformed

    private void txt_raActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_raActionPerformed
  try{
       String sql="select * from car";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfcvabr.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     }
      
    }//GEN-LAST:event_txt_raActionPerformed

    private void txt_rbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rbActionPerformed
    try{
       String sql="select * from cbr";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfcvabr.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     
     }  
    }//GEN-LAST:event_txt_rbActionPerformed

    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new cvabr_operation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txt_id;
    private javax.swing.JTable txt_jtfcvabr;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JButton txt_ra;
    private javax.swing.JButton txt_rb;
    private javax.swing.JButton txt_rv;
    private javax.swing.JTextField txt_title;
    private javax.swing.JButton txt_urna;
    private javax.swing.JButton txt_urnb;
    private javax.swing.JButton txt_urnv;
    private javax.swing.JButton txt_urta;
    private javax.swing.JButton txt_urtb;
    private javax.swing.JButton txt_urtv;
    // End of variables declaration//GEN-END:variables
}
