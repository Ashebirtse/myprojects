import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
public class login_jframe extends javax.swing.JFrame {
   
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
   
 
   /*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
   
   public login_jframe() {
        initComponents();
        conn=javaconnecion.myconnection();
        setTitle("Login page");
        setSize(600,600);
        setLocation(50,50);
        txt_username.setText("aaa");
    }
/*///////////////////////////////////////////////////////////////////////////////////////////////////////////////*/   
public void close(){
WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);

}
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/   
   
   
   
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_username = new javax.swing.JTextField();
        cmd_login = new javax.swing.JButton();
        txt_password = new javax.swing.JPasswordField();
        txt_exit = new javax.swing.JButton();
        txt_clear = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("username");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("password");

        txt_username.setBackground(new java.awt.Color(102, 102, 255));
        txt_username.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_usernameActionPerformed(evt);
            }
        });

        cmd_login.setBackground(new java.awt.Color(51, 51, 255));
        cmd_login.setText("Login");
        cmd_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmd_loginActionPerformed(evt);
            }
        });

        txt_password.setBackground(new java.awt.Color(51, 255, 0));
        txt_password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_passwordKeyPressed(evt);
            }
        });

        txt_exit.setBackground(new java.awt.Color(255, 51, 0));
        txt_exit.setText("exit");
        txt_exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_exitActionPerformed(evt);
            }
        });

        txt_clear.setBackground(new java.awt.Color(51, 51, 255));
        txt_clear.setText("clear");
        txt_clear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_clearActionPerformed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/shebir-circle.jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_clear, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_username, javax.swing.GroupLayout.DEFAULT_SIZE, 118, Short.MAX_VALUE)
                            .addComponent(txt_password)))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmd_login)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_exit)))
                .addGap(56, 56, 56))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_username, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_password)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_clear)
                    .addComponent(cmd_login)
                    .addComponent(txt_exit))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 353, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_usernameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_usernameActionPerformed
       
    }//GEN-LAST:event_txt_usernameActionPerformed

     /*/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void cmd_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmd_loginActionPerformed
     
      try{
          String sql="select * from atable where username=? and password=?";  
          pst=conn.prepareStatement(sql);
          pst.setString(1,txt_username.getText());
          pst.setString(2,txt_password.getText());
          rs=pst.executeQuery();
          if(rs.next())
          {
      /*JOptionPane.showMessageDialog(null,"username and password are correct");
           rs.close();
           pst.close();
           close();
           HOME s=new HOME();
           s.setVisible(true);*/
            dispose();
           new HOME().setVisible(true);
      
          }
          else{
              JOptionPane.showMessageDialog(null,"username and password are not correct");
              }
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
      finally{
       try{
     rs.close();
     pst.close();
       }
       catch(Exception e){
       }
   }
    }//GEN-LAST:event_cmd_loginActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_exitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_txt_exitActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_clearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_clearActionPerformed
        txt_username.setText("");
        txt_password.setText("");
        
    }//GEN-LAST:event_txt_clearActionPerformed

    private void txt_passwordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_passwordKeyPressed
       if(evt.getKeyCode()==KeyEvent.VK_ENTER){
        try{
          String sql="select * from atable where username=? and password=?";  
          pst=conn.prepareStatement(sql);
          pst.setString(1,txt_username.getText());
          pst.setString(2,txt_password.getText());
          rs=pst.executeQuery();
          if(rs.next())
          {
      /*JOptionPane.showMessageDialog(null,"username and password are correct");
           rs.close();
           pst.close();
           close();
           HOME s=new HOME();
           s.setVisible(true);*/
            dispose();
           new HOME().setVisible(true);
      
          }
          else{
              JOptionPane.showMessageDialog(null,"username and password are not correct");
              }
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
      finally{
       try{
     rs.close();
     pst.close();
       }
       catch(Exception e){
       }
   }
       }
    }//GEN-LAST:event_txt_passwordKeyPressed
/*/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/  
    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new login_jframe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmd_login;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JButton txt_clear;
    private javax.swing.JButton txt_exit;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextField txt_username;
    // End of variables declaration//GEN-END:variables
}
