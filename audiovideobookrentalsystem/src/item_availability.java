import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
public class item_availability extends javax.swing.JFrame {
     Connection conn=null;
     ResultSet rs=null;
     PreparedStatement pst=null;
    public item_availability() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(800,400);
        setLocation(400,50);
    }
    public void update(){
       }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_cva = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jtfavailability = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txt_title = new javax.swing.JTextField();
        txt_caa = new javax.swing.JButton();
        txt_cba = new javax.swing.JButton();
        Home = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_cva.setText("check video availability");
        txt_cva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cvaActionPerformed(evt);
            }
        });

        txt_jtfavailability.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(txt_jtfavailability);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("title");

        txt_caa.setText("check audio availability");
        txt_caa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_caaActionPerformed(evt);
            }
        });

        txt_cba.setText("check book availability");
        txt_cba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cbaActionPerformed(evt);
            }
        });

        Home.setText("Home");
        Home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                HomeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txt_cva, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(Home))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_caa, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cba, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 62, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 375, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txt_cva, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Home, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(19, 19, 19)))
                .addComponent(txt_caa, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_cba, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(110, 110, 110))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 99, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_cvaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cvaActionPerformed
      try{
String sql="select * from vtable where title=?";
pst=conn.prepareStatement(sql);
pst.setString(1,txt_title.getText());
rs=pst.executeQuery();
if(rs.next())
{
     try{
String s="select * from vtable where title=?";
pst=conn.prepareStatement(s);
pst.setString(1,txt_title.getText());
rs=pst.executeQuery();
txt_jtfavailability.setModel(DbUtils.resultSetToTableModel(rs));
}
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
}
       else
          JOptionPane.showMessageDialog(null,"a video with this title does not exist");
       }
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
        }        
    }//GEN-LAST:event_txt_cvaActionPerformed

    private void txt_caaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_caaActionPerformed
        try{
            String sql="select * from autable where title=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1,txt_title.getText());
            rs=pst.executeQuery();
            if(rs.next())
              {
               try{
                   String s="select * from autable where title=?";
                   pst=conn.prepareStatement(s);
                   pst.setString(1,txt_title.getText());
                   rs=pst.executeQuery();
                   txt_jtfavailability.setModel(DbUtils.resultSetToTableModel(rs));
                  }
                catch(Exception e)
                  {
                    JOptionPane.showMessageDialog(null, e);
                  }
               }
            else
             JOptionPane.showMessageDialog(null,"an audio with this title does not exist");
           }
        catch(Exception e)
         {
          JOptionPane.showMessageDialog(null, e);
         }
    }//GEN-LAST:event_txt_caaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_cbaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cbaActionPerformed
        try{
            String sql="select * from book where title=?";
            pst=conn.prepareStatement(sql);
            pst.setString(1,txt_title.getText());
            rs=pst.executeQuery();
            if(rs.next())
              {
                try{
                    String s="select * from book where title=?";
                    pst=conn.prepareStatement(s);
                    pst.setString(1,txt_title.getText());
                    rs=pst.executeQuery();
                    txt_jtfavailability.setModel(DbUtils.resultSetToTableModel(rs));
                   }
                catch(Exception e)
                   {
                    JOptionPane.showMessageDialog(null, e);
                   }
              }
            else
              JOptionPane.showMessageDialog(null,"a book with this title does not exist");
            }
        catch(Exception e)
            {
            JOptionPane.showMessageDialog(null, e);
            }
    }//GEN-LAST:event_txt_cbaActionPerformed

    private void HomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_HomeActionPerformed
      dispose();
      new HOME().setVisible(true);
    }//GEN-LAST:event_HomeActionPerformed

/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new item_availability().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Home;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton txt_caa;
    private javax.swing.JButton txt_cba;
    private javax.swing.JButton txt_cva;
    private javax.swing.JTable txt_jtfavailability;
    private javax.swing.JTextField txt_title;
    // End of variables declaration//GEN-END:variables
}
