import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import net.proteanit.sql.DbUtils;
public class video_operation extends javax.swing.JFrame {
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
 /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/   
    public video_operation() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(700,500);
        setLocation(400,50);
        combobox();
        updatejtable();
    }
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void combobox(){
     try{
        String sql="select * from vtable ";  
        pst=conn.prepareStatement(sql);
        rs= pst.executeQuery();
         while(rs.next())
            {
            String add1=rs.getString("title");
            txt_cb.addItem(add1);
            
           }
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }
  /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public void close1(){
   WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
   Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);

}
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
public void updatejtable(){
try{
String sql="select * from vtable";
pst=conn.prepareStatement(sql);
rs=pst.executeQuery();
txt_jtfvideo.setModel(DbUtils.resultSetToTableModel(rs));


}
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
}
   
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_addnv = new javax.swing.JButton();
        txt_updatev = new javax.swing.JButton();
        txt_deleteev = new javax.swing.JButton();
        txt_home = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jtfvideo = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_title = new javax.swing.JTextField();
        txt_amount = new javax.swing.JTextField();
        txt_artist = new javax.swing.JTextField();
        txt_search = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_cb = new javax.swing.JComboBox<>();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_addnv.setBackground(new java.awt.Color(51, 255, 102));
        txt_addnv.setForeground(new java.awt.Color(255, 51, 153));
        txt_addnv.setText("addnv");
        txt_addnv.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_addnvActionPerformed(evt);
            }
        });

        txt_updatev.setBackground(new java.awt.Color(102, 255, 102));
        txt_updatev.setForeground(new java.awt.Color(255, 51, 204));
        txt_updatev.setText("updatev");
        txt_updatev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_updatevActionPerformed(evt);
            }
        });

        txt_deleteev.setBackground(new java.awt.Color(51, 255, 102));
        txt_deleteev.setForeground(new java.awt.Color(255, 51, 204));
        txt_deleteev.setText("deleteev");
        txt_deleteev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_deleteevActionPerformed(evt);
            }
        });

        txt_home.setBackground(new java.awt.Color(51, 255, 51));
        txt_home.setForeground(new java.awt.Color(255, 51, 204));
        txt_home.setText("home");
        txt_home.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_homeActionPerformed(evt);
            }
        });

        txt_jtfvideo.setBackground(new java.awt.Color(51, 255, 255));
        txt_jtfvideo.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        txt_jtfvideo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_jtfvideoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(txt_jtfvideo);

        jLabel1.setBackground(new java.awt.Color(102, 255, 255));
        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 255));
        jLabel1.setText("title");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 0, 255));
        jLabel2.setText("artist");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 0, 255));
        jLabel3.setText("amount");

        txt_title.setBackground(new java.awt.Color(153, 255, 0));

        txt_amount.setBackground(new java.awt.Color(0, 255, 0));
        txt_amount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_amountActionPerformed(evt);
            }
        });

        txt_artist.setBackground(new java.awt.Color(0, 255, 0));

        txt_search.setBackground(new java.awt.Color(102, 255, 0));
        txt_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_searchActionPerformed(evt);
            }
        });
        txt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_searchKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_searchKeyReleased(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("title");

        txt_cb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cbActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.setText("search");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(49, 49, 49)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_deleteev)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                    .addComponent(txt_addnv, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                    .addComponent(txt_updatev, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addGap(55, 55, 55)
                                                .addComponent(txt_home))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(38, 38, 38)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(6, 6, 6)
                                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txt_artist, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel5)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(14, 14, 14)
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGap(31, 31, 31))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton1)
                                .addGap(93, 93, 93)))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(316, 316, 316)
                        .addComponent(txt_cb, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(305, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txt_addnv))
                            .addComponent(txt_home))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_updatev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_deleteev)
                        .addGap(44, 44, 44)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt_artist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(52, 52, 52)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(txt_cb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_addnvActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_addnvActionPerformed
     if(txt_title.equals(""))
      JOptionPane.showMessageDialog(null,"no selected item");
      else
     {
    try{
      String sql="insert into vtable(title,artist,amount)values(?,?,?) ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_title.getText());
      pst.setString(2,txt_artist.getText());
      pst.setString(3,txt_amount.getText());
      pst.execute();
      JOptionPane.showMessageDialog(null,"updated successfully");
         }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }
              updatejtable();
    }//GEN-LAST:event_txt_addnvActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_homeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_homeActionPerformed
       
      dispose();
      new HOME().setVisible(true);
    }//GEN-LAST:event_txt_homeActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_updatevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_updatevActionPerformed
     try{
        String s="select * from vtable where title=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_title.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
        try{
      String sql="update vtable set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_amount.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
      }
     }
        else{
             JOptionPane.showMessageDialog(null,"there is no video with this title");   
           }
     }
      catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
}     
updatejtable();
    }//GEN-LAST:event_txt_updatevActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_deleteevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_deleteevActionPerformed
     int p=JOptionPane.showConfirmDialog(null,"Do you really want to delete?","Delete",JOptionPane.YES_NO_OPTION);
      if(p==0){   
        try{
        String s="select * from vtable where title=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_title.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
        try{
      String sql="delete from vtable where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"deleted sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
      }
      }
        else
            JOptionPane.showMessageDialog(null,"there is no video with this title");  
       }
       catch(Exception e){
       JOptionPane.showMessageDialog(null,e);
       }
       updatejtable();
      }
    }//GEN-LAST:event_txt_deleteevActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_jtfvideoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_jtfvideoMouseClicked
       try{
            int row=txt_jtfvideo.getSelectedRow();
            String table_click=(txt_jtfvideo.getModel().getValueAt(row,2).toString());
            String sql="select * from vtable where amount="+table_click+"";
            pst=conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if(rs.next())
            {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("artist");
            txt_artist.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
           }
            
        }
        catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_txt_jtfvideoMouseClicked

    private void txt_searchKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_searchKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_searchKeyPressed

    private void txt_searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_searchKeyReleased
        
    }//GEN-LAST:event_txt_searchKeyReleased

    private void txt_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_searchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_searchActionPerformed

    private void txt_amountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_amountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_amountActionPerformed

    private void txt_cbActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cbActionPerformed
        
    }//GEN-LAST:event_txt_cbActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try{
        String s="select * from vtable where title=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_search.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("artist");
            txt_artist.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
        }
       else
            JOptionPane.showMessageDialog(null,"video with this title does not exist");
       }
       catch(Exception e){
       JOptionPane.showMessageDialog(null,e);
       }
    }//GEN-LAST:event_jButton1ActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
   
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new video_operation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton txt_addnv;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_artist;
    private javax.swing.JComboBox<String> txt_cb;
    private javax.swing.JButton txt_deleteev;
    private javax.swing.JButton txt_home;
    private javax.swing.JTable txt_jtfvideo;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_title;
    private javax.swing.JButton txt_updatev;
    // End of variables declaration//GEN-END:variables

}
