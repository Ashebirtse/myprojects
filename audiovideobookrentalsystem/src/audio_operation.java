import java.sql.*;
import java.awt.event.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
public class audio_operation extends javax.swing.JFrame {
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
public audio_operation() {
        initComponents();
        conn=javaconnecion.myconnection();
        setSize(700,500);
        setLocation(400,50);
        updatejtable();
    }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
 public void close(){
   WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
   Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);

}
    public void updatejtable(){
   try{
       String sql="select * from autable";
       pst=conn.prepareStatement(sql);
       rs=pst.executeQuery();
       txt_jtfaudio.setModel(DbUtils.resultSetToTableModel(rs));


      }
   catch(Exception e){
      JOptionPane.showMessageDialog(null, e);
     }
     }  
 
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_addna = new javax.swing.JButton();
        txt_updatea = new javax.swing.JButton();
        txt_deleteea = new javax.swing.JButton();
        txt_hpme = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_jtfaudio = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_artist = new javax.swing.JTextField();
        txt_amount = new javax.swing.JTextField();
        txt_title = new javax.swing.JTextField();
        txt_search = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_addna.setBackground(new java.awt.Color(51, 51, 255));
        txt_addna.setForeground(new java.awt.Color(255, 51, 153));
        txt_addna.setText("addna");
        txt_addna.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_addnaActionPerformed(evt);
            }
        });

        txt_updatea.setBackground(new java.awt.Color(51, 51, 255));
        txt_updatea.setForeground(new java.awt.Color(255, 51, 204));
        txt_updatea.setText("updatea");
        txt_updatea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_updateaActionPerformed(evt);
            }
        });

        txt_deleteea.setBackground(new java.awt.Color(51, 51, 255));
        txt_deleteea.setForeground(new java.awt.Color(255, 0, 204));
        txt_deleteea.setText("deleteea");
        txt_deleteea.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_deleteeaActionPerformed(evt);
            }
        });

        txt_hpme.setBackground(new java.awt.Color(51, 51, 255));
        txt_hpme.setForeground(new java.awt.Color(255, 51, 153));
        txt_hpme.setText("home");
        txt_hpme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_hpmeActionPerformed(evt);
            }
        });

        txt_jtfaudio.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        txt_jtfaudio.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txt_jtfaudioMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(txt_jtfaudio);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("title");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("artist");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setText("amount");

        txt_artist.setBackground(new java.awt.Color(51, 51, 255));

        txt_amount.setBackground(new java.awt.Color(51, 51, 255));

        txt_title.setBackground(new java.awt.Color(51, 51, 255));
        txt_title.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_titleActionPerformed(evt);
            }
        });

        txt_search.setBackground(new java.awt.Color(51, 51, 255));
        txt_search.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_searchKeyReleased(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel4.setText("search");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setText("title");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(134, 134, 134))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_artist, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(87, 87, 87))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(32, 32, 32)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_deleteea)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txt_addna, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_updatea, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(41, 41, 41)
                                        .addComponent(txt_hpme))))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 363, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(242, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(txt_addna))
                            .addComponent(txt_hpme))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_updatea)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_deleteea)
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt_artist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(txt_amount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(67, 67, 67)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 304, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(112, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_hpmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_hpmeActionPerformed
        dispose();
        new HOME().setVisible(true);
    }//GEN-LAST:event_txt_hpmeActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_addnaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_addnaActionPerformed
         try{
             String sql="insert into autable(title,artist,amount)values(?,?,?) ";  
             pst=conn.prepareStatement(sql);
             pst.setString(1,txt_title.getText());
             pst.setString(2,txt_artist.getText());
             pst.setString(3,txt_amount.getText());
             pst.execute();
      
             JOptionPane.showMessageDialog(null,"registered sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
         updatejtable();
      }//GEN-LAST:event_txt_addnaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_updateaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_updateaActionPerformed
    try{
      String sql="update autable set amount=amount+? where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_amount.getText());
      pst.setString(2,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    updatejtable();
    }//GEN-LAST:event_txt_updateaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_deleteeaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_deleteeaActionPerformed
  int p=JOptionPane.showConfirmDialog(null,"Do you really want to delete?","Delete",JOptionPane.YES_NO_OPTION);
   if(p==0){  
    try{
      String sql="delete from autable where title=? ";  
      pst=conn.prepareStatement(sql);
      pst.setString(1,txt_title.getText());
      pst.execute();
      
      JOptionPane.showMessageDialog(null,"deleted sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    updatejtable();
   }
    }//GEN-LAST:event_txt_deleteeaActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_jtfaudioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txt_jtfaudioMouseClicked
        try{
            int row=txt_jtfaudio.getSelectedRow();
            String table_click=(txt_jtfaudio.getModel().getValueAt(row,2).toString());
            String sql="select * from autable where amount="+table_click+"";
            pst=conn.prepareStatement(sql);
            rs=pst.executeQuery();
            if(rs.next())
            {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("artist");
            txt_artist.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
            }
            
        }
        catch(Exception e){
        JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_txt_jtfaudioMouseClicked

    private void txt_searchKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_searchKeyReleased
        try{
        String s="select * from autable where title=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_search.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
            String add1=rs.getString("title");
            txt_title.setText(add1);
            String add2=rs.getString("artist");
            txt_artist.setText(add2);
            String add3=rs.getString("amount");
            txt_amount.setText(add3);
        }
         
       }
       catch(Exception e){
       JOptionPane.showMessageDialog(null,e);
       }
    }//GEN-LAST:event_txt_searchKeyReleased

    private void txt_titleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_titleActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_titleActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new audio_operation().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton txt_addna;
    private javax.swing.JTextField txt_amount;
    private javax.swing.JTextField txt_artist;
    private javax.swing.JButton txt_deleteea;
    private javax.swing.JButton txt_hpme;
    private javax.swing.JTable txt_jtfaudio;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_title;
    private javax.swing.JButton txt_updatea;
    // End of variables declaration//GEN-END:variables
}
