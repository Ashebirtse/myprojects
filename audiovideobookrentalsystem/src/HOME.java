import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
public class HOME extends javax.swing.JFrame {
    Connection conn;
    ResultSet rs;
    PreparedStatement pst;
    public HOME() {
       initComponents();
       currentaate();
       setSize(800,500);
       setLocation(50,50);
    }
    public void currentaate(){
    Calendar cal=new GregorianCalendar();
    int year=cal.get(Calendar.YEAR);
    int month=cal.get(Calendar.MONTH);
    int day=cal.get(Calendar.DAY_OF_MONTH);
    txt_date.setText(year+"/"+(month+1)+"/"+day);
    
    }
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
  public void close(){
   WindowEvent wce=new WindowEvent(this,WindowEvent.WINDOW_CLOSING);
   Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wce);

}
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        txt_customer = new javax.swing.JButton();
        txt_video = new javax.swing.JButton();
        txt_audio = new javax.swing.JButton();
        txt_book = new javax.swing.JButton();
        txt_renteditem = new javax.swing.JButton();
        txt_admin = new javax.swing.JButton();
        txt_availability = new javax.swing.JButton();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        txt_date = new javax.swing.JMenu();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setBackground(new java.awt.Color(255, 204, 204));
        setFont(new java.awt.Font("Agency FB", 0, 24)); // NOI18N

        txt_customer.setBackground(new java.awt.Color(255, 204, 204));
        txt_customer.setText("customer");
        txt_customer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_customerActionPerformed(evt);
            }
        });

        txt_video.setBackground(new java.awt.Color(255, 204, 204));
        txt_video.setText("video");
        txt_video.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_videoActionPerformed(evt);
            }
        });

        txt_audio.setBackground(new java.awt.Color(255, 204, 204));
        txt_audio.setForeground(new java.awt.Color(0, 51, 51));
        txt_audio.setText("audio");
        txt_audio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_audioActionPerformed(evt);
            }
        });

        txt_book.setBackground(new java.awt.Color(255, 0, 204));
        txt_book.setText("book");
        txt_book.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_bookActionPerformed(evt);
            }
        });

        txt_renteditem.setBackground(new java.awt.Color(204, 255, 255));
        txt_renteditem.setText("renteditem");
        txt_renteditem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_renteditemActionPerformed(evt);
            }
        });

        txt_admin.setBackground(new java.awt.Color(204, 255, 255));
        txt_admin.setText("admin");
        txt_admin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_adminActionPerformed(evt);
            }
        });

        txt_availability.setBackground(new java.awt.Color(153, 255, 255));
        txt_availability.setText("availability");
        txt_availability.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_availabilityActionPerformed(evt);
            }
        });

        jTextField1.setBackground(new java.awt.Color(102, 255, 102));
        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jTextField1.setText("         Home_page");

        jButton1.setText("open web page");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jMenuBar1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N

        jMenu1.setBackground(new java.awt.Color(255, 0, 204));
        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setBackground(new java.awt.Color(255, 0, 204));
        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        txt_date.setBackground(new java.awt.Color(255, 51, 204));
        txt_date.setText("date");
        jMenuBar1.add(txt_date);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTextField1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_customer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_renteditem, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_admin, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_availability, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_video, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_audio, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_book, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 71, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_customer)
                    .addComponent(txt_video)
                    .addComponent(txt_audio))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_renteditem)
                            .addComponent(txt_admin)
                            .addComponent(txt_availability)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(txt_book)))
                .addGap(29, 29, 29)
                .addComponent(jButton1)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
 /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_customerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_customerActionPerformed
 customer_operation s=new customer_operation();
     s.setVisible(true);
     close();
    }//GEN-LAST:event_txt_customerActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_videoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_videoActionPerformed
            video_operation s=new video_operation();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_videoActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_audioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_audioActionPerformed
       
        audio_operation s=new audio_operation();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_audioActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_bookActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_bookActionPerformed
            book_operation s=new book_operation();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_bookActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_renteditemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_renteditemActionPerformed
            cvabr_operation s=new cvabr_operation();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_renteditemActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_adminActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_adminActionPerformed
        owner_operation s=new owner_operation();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_adminActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    private void txt_availabilityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_availabilityActionPerformed
     item_availability s=new item_availability();
            s.setVisible(true);
            close();
    }//GEN-LAST:event_txt_availabilityActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       try{
           String URL="https://www.facebook.com";
        java.awt.Desktop.getDesktop().browse(java.net.URI.create(URL));
    }
        catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
        
    }//GEN-LAST:event_jButton1ActionPerformed
/*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
            new HOME().setVisible(true);
            
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JButton txt_admin;
    private javax.swing.JButton txt_audio;
    private javax.swing.JButton txt_availability;
    private javax.swing.JButton txt_book;
    private javax.swing.JButton txt_customer;
    private javax.swing.JMenu txt_date;
    private javax.swing.JButton txt_renteditem;
    private javax.swing.JButton txt_video;
    // End of variables declaration//GEN-END:variables
}
