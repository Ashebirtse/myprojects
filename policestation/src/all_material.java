import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
import sun.audio.*;
import sun.audio.AudioStream;
import javax.swing.JOptionPane;
public class all_material extends javax.swing.JFrame {
    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;
   
    public all_material() {
        initComponents();
        conn=connectionclass.myconnection();
    }

   
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_aynet_nbret = new javax.swing.JTextField();
        txt_zhbo_agelglot = new javax.swing.JTextField();
        txt_zrkebelu_keydisrah = new javax.swing.JTextField();
        txt_ztegezalu_waga = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_mefleyi_kutsri = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_aynet_nbret1 = new javax.swing.JTextField();
        txt_zrkebelu_keydisrah1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel1.setText("ዓይነት ንብረት");

        jLabel2.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel2.setText("ዝህቦ ኣገልግሎት");

        jLabel3.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel3.setText("ዝተገእሉ ዋጋ");

        jLabel4.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel4.setText("ዝርከበሉ ከይዲ ስራሕ");

        txt_aynet_nbret.setBackground(new java.awt.Color(0, 255, 255));
        txt_aynet_nbret.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_aynet_nbret.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aynet_nbretActionPerformed(evt);
            }
        });

        txt_zhbo_agelglot.setBackground(new java.awt.Color(0, 255, 255));
        txt_zhbo_agelglot.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_zhbo_agelglot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_zhbo_agelglotActionPerformed(evt);
            }
        });

        txt_zrkebelu_keydisrah.setBackground(new java.awt.Color(0, 255, 255));
        txt_zrkebelu_keydisrah.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_zrkebelu_keydisrah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_zrkebelu_keydisrahActionPerformed(evt);
            }
        });

        txt_ztegezalu_waga.setBackground(new java.awt.Color(0, 255, 255));
        txt_ztegezalu_waga.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_ztegezalu_waga.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ztegezalu_wagaActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel5.setText("መፍለዪ ቁፅሪ");

        txt_mefleyi_kutsri.setBackground(new java.awt.Color(51, 204, 255));
        txt_mefleyi_kutsri.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_mefleyi_kutsri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mefleyi_kutsriActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel6.setText("ዓይነት ንብረት");

        jLabel7.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel7.setText("ዝርከበሉ ከይዲ ስራሕ");

        txt_aynet_nbret1.setBackground(new java.awt.Color(51, 204, 255));
        txt_aynet_nbret1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_aynet_nbret1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aynet_nbret1ActionPerformed(evt);
            }
        });

        txt_zrkebelu_keydisrah1.setBackground(new java.awt.Color(51, 204, 255));
        txt_zrkebelu_keydisrah1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_zrkebelu_keydisrah1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_zrkebelu_keydisrah1ActionPerformed(evt);
            }
        });

        jButton1.setBackground(new java.awt.Color(0, 255, 255));
        jButton1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton1.setText("መዝግብ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setBackground(new java.awt.Color(0, 204, 255));
        jButton2.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton2.setText("ኣስተኻኽል");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setBackground(new java.awt.Color(0, 204, 255));
        jButton3.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton3.setText("ደምስስ");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setBackground(new java.awt.Color(0, 204, 255));
        jButton4.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton4.setText("ድለይ");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setBackground(new java.awt.Color(0, 255, 255));
        jButton5.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton5.setText("exit");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setBackground(new java.awt.Color(0, 255, 255));
        jButton6.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton6.setText("previos-page");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setBackground(new java.awt.Color(0, 204, 255));
        jButton7.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton7.setText("ውፃእ");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jLabel8.setBackground(new java.awt.Color(0, 255, 255));
        jLabel8.setFont(new java.awt.Font("Nyala", 0, 24)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 204, 255));
        jLabel8.setText("ንምስትኽኻል ዝስዕብ ፎርም ይምልኡ");

        jLabel9.setBackground(new java.awt.Color(0, 255, 255));
        jLabel9.setFont(new java.awt.Font("Nyala", 0, 24)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 255, 0));
        jLabel9.setText("ናይ ጠቅላላ ኣቑሑ መመዝገቢ ቅጥዒ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton7)
                        .addGap(118, 118, 118))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_aynet_nbret))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_ztegezalu_waga)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(0, 0, Short.MAX_VALUE))
                                            .addComponent(txt_zrkebelu_keydisrah)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_zhbo_agelglot)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(262, 262, 262)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jButton6)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_aynet_nbret1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_zrkebelu_keydisrah1, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                        .addGap(29, 29, 29))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(85, 85, 85)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(525, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_aynet_nbret, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_aynet_nbret1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_zrkebelu_keydisrah1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_zhbo_agelglot, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_ztegezalu_waga, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_zrkebelu_keydisrah, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(76, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(21, 21, 21)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(421, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_aynet_nbretActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aynet_nbretActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aynet_nbretActionPerformed

    private void txt_zhbo_agelglotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_zhbo_agelglotActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_zhbo_agelglotActionPerformed

    private void txt_zrkebelu_keydisrahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_zrkebelu_keydisrahActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_zrkebelu_keydisrahActionPerformed

    private void txt_ztegezalu_wagaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ztegezalu_wagaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_ztegezalu_wagaActionPerformed

    private void txt_mefleyi_kutsriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mefleyi_kutsriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_mefleyi_kutsriActionPerformed

    private void txt_aynet_nbret1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aynet_nbret1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aynet_nbret1ActionPerformed

    private void txt_zrkebelu_keydisrah1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_zrkebelu_keydisrah1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_zrkebelu_keydisrah1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       try{
        String sql="insert into teklala_nbret(aynet_nbret,zhbo_agelglot,waga,zrkebelu_keydi_srah) values(?,?,?,?)";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_aynet_nbret.getText());
        pst.setString(2,txt_zhbo_agelglot.getText());
        pst.setString(3,txt_ztegezalu_waga.getText());
        pst.setString(4,txt_zrkebelu_keydisrah.getText());
        pst.execute();
      
        JOptionPane.showMessageDialog(null,"registered sucessfully");
         
      }
      catch(Exception e)
       {
         JOptionPane.showMessageDialog(null,e);
       
       }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try{
        String s="select * from teklala_nbret where id=? and aynet_nbret=? and zrkebelu_keydi_srah=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_mefleyi_kutsri.getText());
        pst.setString(2,txt_aynet_nbret1.getText());
        pst.setString(3,txt_zrkebelu_keydisrah1.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
            String add1=rs.getString("aynet_nbret");
            txt_aynet_nbret.setText(add1);
            String add2=rs.getString("zhbo_agelglot");
            txt_zhbo_agelglot.setText(add2);
            String add3=rs.getString("waga");
            txt_ztegezalu_waga.setText(add3);
            String add4=rs.getString("zrkebelu_keydi_srah");
            txt_zrkebelu_keydisrah.setText(add4);
        }  
        }
        catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
}  
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            String s="select * from teklala_nbret where id=? and aynet_nbret=? and zrkebelu_keydi_srah=? ";  
            pst=conn.prepareStatement(s);
            pst.setString(1,txt_mefleyi_kutsri.getText());
            pst.setString(2,txt_aynet_nbret1.getText());
            pst.setString(3,txt_zrkebelu_keydisrah1.getText());
            rs=pst.executeQuery();
        if(rs.next())
        {
           try{
                String sql="update teklala_nbret set aynet_nbret=?, zhbo_agelglot=?, waga=?, zrkebelu_keydi_srah=? where id=? ";  
                pst=conn.prepareStatement(sql);
                pst.setString(1,txt_aynet_nbret.getText());
                pst.setString(2,txt_zhbo_agelglot.getText());
                pst.setString(3,txt_ztegezalu_waga.getText());
                pst.setString(4,txt_zrkebelu_keydisrah.getText());
                pst.setString(5,txt_mefleyi_kutsri.getText());
                pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
      }
     }
        else{
             JOptionPane.showMessageDialog(null,"there is no material with those specification");   
           }
     }
      catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
}     
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try{
                String s="select * from teklala_nbret where id=? and aynet_nbret=? and zrkebelu_keydi_srah=? ";  
                pst=conn.prepareStatement(s);
                pst.setString(1,txt_mefleyi_kutsri.getText());
                pst.setString(2,txt_aynet_nbret1.getText());
                pst.setString(3,txt_zrkebelu_keydisrah1.getText());
                rs=pst.executeQuery();
        if(rs.next())
            { 
            try{
                String sql="delete from teklala_nbret where id=? and aynet_nbret=? and zrkebelu_keydi_srah=? ";  
                pst=conn.prepareStatement(sql);
                pst.setString(1,txt_mefleyi_kutsri.getText());
                pst.setString(2,txt_aynet_nbret1.getText());
                pst.setString(3,txt_zrkebelu_keydisrah1.getText());
                pst.execute();
      
      JOptionPane.showMessageDialog(null,"deleted sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
      }
      }
        else
            JOptionPane.showMessageDialog(null,"there is no material with those specifications");  
       }
       catch(Exception e){
       JOptionPane.showMessageDialog(null,e);
       }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        dispose();
        new Home_page_for_manpower().setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        dispose();
        new loginframe().setVisible(true);
    }//GEN-LAST:event_jButton7ActionPerformed

    public static void main(String args[]) {
       
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new all_material().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txt_aynet_nbret;
    private javax.swing.JTextField txt_aynet_nbret1;
    private javax.swing.JTextField txt_mefleyi_kutsri;
    private javax.swing.JTextField txt_zhbo_agelglot;
    private javax.swing.JTextField txt_zrkebelu_keydisrah;
    private javax.swing.JTextField txt_zrkebelu_keydisrah1;
    private javax.swing.JTextField txt_ztegezalu_waga;
    // End of variables declaration//GEN-END:variables
}
