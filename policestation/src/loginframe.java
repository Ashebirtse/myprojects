import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.sql.*;
import javax.swing.*;
import net.proteanit.sql.DbUtils;
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import net.proteanit.sql.DbUtils;
public class loginframe extends javax.swing.JFrame {
   Connection conn=null;
   ResultSet rs=null;
   PreparedStatement pst=null;
   
 
   /*///////////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
    public loginframe() {
        initComponents();
        conn=connectionclass.myconnection();
        setTitle("Police Station Management System");
        setSize(1000,800);
        setLocation(50,50);
        combobox();
    }
    private void combobox(){
     try{
        String sql="select * from user_table ";  
        pst=conn.prepareStatement(sql);
        rs= pst.executeQuery();
         while(rs.next())
            {
            String add1=rs.getString("user_type");
            txt_usertype.addItem(add1);
            
           }
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }
   
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_username = new javax.swing.JTextPane();
        jLabel3 = new javax.swing.JLabel();
        txt_login = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txt_usertype = new javax.swing.JComboBox<>();
        txt_password = new javax.swing.JPasswordField();
        txt_login1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 153, 204));

        jLabel2.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel2.setText("ሽም ተጠቃሚ");

        txt_username.setBackground(new java.awt.Color(0, 153, 204));
        txt_username.setFont(new java.awt.Font("Nyala", 1, 14)); // NOI18N
        jScrollPane2.setViewportView(txt_username);

        jLabel3.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel3.setText("ምስጢር ቁፅሪ");

        txt_login.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_login.setText("እተው");
        txt_login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_loginActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 255));
        jLabel4.setText("Login page");

        jLabel5.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel5.setText("ዓይነት ተጠቃሚ");

        txt_usertype.setEditable(true);
        txt_usertype.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_usertype.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "admin", "crime_protection", "crime_investigation", "information_and_monitoring", "man_power", "people_organization_and_education", " ", " " }));

        txt_password.setBackground(new java.awt.Color(0, 153, 204));
        txt_password.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        txt_login1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_login1.setText("cancel");
        txt_login1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_login1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(258, 258, 258)
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(151, 151, 151)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 81, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_password)
                                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE))))
                        .addGroup(layout.createSequentialGroup()
                            .addGap(140, 140, 140)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txt_usertype, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(276, 276, 276)
                        .addComponent(txt_login, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_login1, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(130, 217, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_password, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_usertype, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_login1, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_login, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_loginActionPerformed
        
        try{
          String sql="select * from user_table where user_type=? and username=? and password=?";  
          pst=conn.prepareStatement(sql);
          String value=txt_usertype.getSelectedItem().toString();
          pst.setString(1,value);
          pst.setString(2,txt_username.getText());
          pst.setString(3,txt_password.getText());
          rs=pst.executeQuery();
          if(rs.next())
          {
            JOptionPane.showMessageDialog(null,"username and password are correct");
            if(value=="man_power"){
             dispose();
             new Home_page_for_manpower().setVisible(true);
            }
            else if(value=="information_and_monitoring"){
             dispose();
             new information_and_monitoring_home_page().setVisible(true);
            }
            else if(value=="admin"){
             dispose();
             new admin_home_page().setVisible(true);
            }
            else if(value=="crime_protection"){
             dispose();
             new crime_protection_home_page().setVisible(true);
            }
            else if(value=="crime_investigation"){
             dispose();
             new crime_investigation_home_page().setVisible(true);
            }
            else if(value=="people_organization_and_education"){
             dispose();
             new people_organization_and_education_home_page().setVisible(true);
            }
          
           }
          else{
              JOptionPane.showMessageDialog(null,"username and password are not correct");
              }
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
       
      }
    }//GEN-LAST:event_txt_loginActionPerformed

    private void txt_login1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_login1ActionPerformed
        txt_username.setText("");
        txt_password.setText("");
        //txt_username.setText("");
    }//GEN-LAST:event_txt_login1ActionPerformed

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new loginframe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton txt_login;
    private javax.swing.JButton txt_login1;
    private javax.swing.JPasswordField txt_password;
    private javax.swing.JTextPane txt_username;
    private javax.swing.JComboBox<String> txt_usertype;
    // End of variables declaration//GEN-END:variables
}
