import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import net.proteanit.sql.DbUtils;
import sun.audio.*;
import sun.audio.AudioStream;
public class criminal_detail extends javax.swing.JFrame {
    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;
    public criminal_detail() {
        initComponents();
        conn=connectionclass.myconnection();
        setTitle("Police Station Management System");
        //setSize(1200,800);
        //setLocation(50,50);
        //showdate();
        //updatejtable();
    }
    void showdate(){
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
   LocalDateTime now = LocalDateTime.now();
   System.out.println(dtf.format(now));
    
    }
    /*//////////////////////////////////////////////////////////////////////////////////////////////////////////////*/
/*public void updatejtable(){
try{
String sql="select * from prisoner_detail order by pid";
pst=conn.prepareStatement(sql);
rs=pst.executeQuery();
txt_jtable.setModel(DbUtils.resultSetToTableModel(rs));


}
catch(Exception e){
JOptionPane.showMessageDialog(null, e);
}
}*/  
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txt_mefleyi_kutsri = new javax.swing.JTextField();
        txt_sm = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_sm_abo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_sm_abohago = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_edme = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_aynet_geben = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_print = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        txt_mefleyi_kutsri1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_sm1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_sm_abo1 = new javax.swing.JTextField();
        txt_sm_abohago1 = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txt_elet1 = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_wereda = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_kebelie = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txt_kushet = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_tsota = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txt_dereja_tmhrti = new javax.swing.JTextField();
        txt_ziegnet = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txt_haymanot = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txt_srah_kunetat = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txt_kutsri_slki = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_mknyat_geben = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txt_kesasi_mefleyi_kutsri = new javax.swing.JTextField();
        jButton9 = new javax.swing.JButton();
        jLabel17 = new javax.swing.JLabel();
        txt_ametemhret = new javax.swing.JTextField();
        jDateChooser1 = new com.toedter.calendar.JDateChooser();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setFont(new java.awt.Font("Nyala", 0, 10)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Nyala", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 255));
        jLabel1.setText("ናይ ገበነኛ መመዝገቢ ቅጥዒ");

        jButton1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton1.setText("መዝግብ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton2.setText("ኣስተኻኽል");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        txt_mefleyi_kutsri.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_mefleyi_kutsri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mefleyi_kutsriActionPerformed(evt);
            }
        });

        txt_sm.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_sm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_smActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel2.setText("መፍለዪ ቁፅሪ");

        jLabel3.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel3.setText("ሽም");

        jLabel4.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel4.setText("ሽም ኣቦ");

        txt_sm_abo.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel5.setText("ሽም ኣቦሓጎ");

        txt_sm_abohago.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_sm_abohago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sm_abohagoActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel6.setText("ዕድመ");

        txt_edme.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_edme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_edmeActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel7.setText("ዓይነት ገበን");

        txt_aynet_geben.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_aynet_geben.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_aynet_gebenActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel10.setText("ዕለትን ሰዓትን");

        txt_print.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_print.setText("ፕሪንት");
        txt_print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_printActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel9.setText("መፍለዪ ቁፅሪ");

        txt_mefleyi_kutsri1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel12.setText("ስም");

        txt_sm1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel13.setText("ስም ኣቦ");

        txt_sm_abo1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        txt_sm_abohago1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel14.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel14.setText("ስም ኣቦሓጎ");

        txt_elet1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel15.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel15.setText("ዕለት");

        jButton4.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton4.setText("ድለይ");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Nyala", 0, 18)); // NOI18N
        jLabel16.setText("ከስተኻኽልዎ ናይ ዝደልዩ ገበነኛ ፎርም ይምልኡ!");

        jLabel18.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel18.setText("ወረዳ");

        txt_wereda.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_wereda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_weredaActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel19.setText("ቀበሌ");

        txt_kebelie.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kebelie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kebelieActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel20.setText("ቁሸት");

        txt_kushet.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kushet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kushetActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel21.setText("ፆታ");

        txt_tsota.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_tsota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_tsotaActionPerformed(evt);
            }
        });

        jLabel22.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel22.setText("ደረጃ ትምህርቲ");

        txt_dereja_tmhrti.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_dereja_tmhrti.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_dereja_tmhrtiActionPerformed(evt);
            }
        });

        txt_ziegnet.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_ziegnet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ziegnetActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel23.setText("ዜግነት");

        txt_haymanot.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_haymanot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_haymanotActionPerformed(evt);
            }
        });

        jLabel24.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel24.setText("ሃይማኖት");

        txt_srah_kunetat.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_srah_kunetat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_srah_kunetatActionPerformed(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel25.setText("ስራሕ_ኩነታት");

        jLabel26.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel26.setText("ቁፅሪ ስልኪ");

        txt_kutsri_slki.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kutsri_slki.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kutsri_slkiActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel8.setText("ምኽንያት ገበን");

        txt_mknyat_geben.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_mknyat_geben.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mknyat_gebenActionPerformed(evt);
            }
        });

        jLabel11.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel11.setText("ከሳሲ መፍለዪ ቁፅሪ");

        txt_kesasi_mefleyi_kutsri.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kesasi_mefleyi_kutsri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kesasi_mefleyi_kutsriActionPerformed(evt);
            }
        });

        jButton9.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton9.setText("ውፃእ");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jLabel17.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel17.setText("ዓመተ ምህረት");

        txt_ametemhret.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_ametemhret.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ametemhretActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(33, 33, 33)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_mefleyi_kutsri)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(txt_sm, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txt_kushet))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                    .addComponent(txt_wereda))
                                                .addGroup(layout.createSequentialGroup()
                                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGap(6, 6, 6)
                                                    .addComponent(txt_kebelie, javax.swing.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(1, 1, 1)
                                                .addComponent(txt_sm_abo, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txt_sm_abohago, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_tsota)
                                            .addComponent(txt_edme))))))
                        .addGap(29, 29, 29))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_print, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(86, 86, 86)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGap(17, 17, 17)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGap(17, 17, 17)
                                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(35, 35, 35)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGap(35, 35, 35)
                                    .addComponent(jLabel24)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(17, 17, 17)
                                .addComponent(jLabel26)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_haymanot)
                            .addComponent(txt_dereja_tmhrti)
                            .addComponent(txt_ziegnet)
                            .addComponent(txt_srah_kunetat)
                            .addComponent(txt_kutsri_slki)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(txt_mknyat_geben, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel10))
                                .addGap(10, 10, 10)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_aynet_geben)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel11)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_ametemhret)
                            .addComponent(txt_kesasi_mefleyi_kutsri))))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(txt_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(45, 45, 45)
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_sm1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_sm_abo1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(31, 31, 31)
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_sm_abohago1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_elet1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(28, 28, 28))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton9)
                                .addGap(45, 45, 45)))
                        .addGap(142, 142, 142))))
            .addGroup(layout.createSequentialGroup()
                .addGap(161, 161, 161)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 535, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_sm, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(6, 6, 6)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_sm_abo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(6, 6, 6)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_sm_abohago, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(12, 12, 12)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_wereda, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_kebelie)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_kushet)
                                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGap(5, 5, 5)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txt_edme, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel22)
                                    .addComponent(txt_dereja_tmhrti, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_ziegnet, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel24)
                                    .addComponent(txt_haymanot, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_srah_kunetat, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_kutsri_slki)
                                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jDateChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_aynet_geben)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_mknyat_geben, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(12, 12, 12)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_tsota, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt_kesasi_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(14, 14, 14)
                                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_ametemhret, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addGap(18, 18, 18)
                        .addComponent(txt_print)
                        .addGap(922, 922, 922))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm_abo1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm_abohago1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_elet1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       try{
        String sql="insert into criminal(id,fname,lname,gf_name,wereda,kebelie,kushet,age,gender,education_level,citizenship,religion,"
                + "work_status,phone_number,date_and_time,crime_type,crime_reason,complaint_id,year)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_mefleyi_kutsri.getText());
        pst.setString(2,txt_sm.getText());
        pst.setString(3,txt_sm_abo.getText());
        pst.setString(4,txt_sm_abohago.getText());
        pst.setString(5,txt_wereda.getText());
        pst.setString(6,txt_kebelie.getText());
        pst.setString(7,txt_kushet.getText());
        pst.setString(8,txt_edme.getText());
        pst.setString(9,txt_tsota.getText());
        pst.setString(10,txt_dereja_tmhrti.getText());
        pst.setString(11,txt_ziegnet.getText());
        pst.setString(12,txt_haymanot.getText());
        pst.setString(13,txt_srah_kunetat.getText());
        pst.setString(14,txt_kutsri_slki.getText());
        pst.setString(15,((JTextField)txt_elet.getDateEditor().getUiComponent()).getText());
        pst.setString(16,txt_aynet_geben.getText());
        pst.setString(17,txt_mknyat_geben.getText());
        pst.setString(18,txt_kesasi_mefleyi_kutsri.getText());
        pst.setString(19,txt_ametemhret.getText());
        
       // pst.setString(19,txt_kesasi_id.getText());
        pst.execute();
      
        JOptionPane.showMessageDialog(null,"registered sucessfully");
      
         // updatejtable();
      }
      catch(Exception e)
       {
         JOptionPane.showMessageDialog(null,e);
       
       }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_smActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_smActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_smActionPerformed

    private void txt_sm_abohagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sm_abohagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sm_abohagoActionPerformed

    private void txt_edmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_edmeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_edmeActionPerformed

    private void txt_aynet_gebenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_aynet_gebenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_aynet_gebenActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
     try{
        String s="select * from criminal where id=? and fname=? and lname=? and gf_name=?";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_mefleyi_kutsri1.getText());
        pst.setString(2,txt_sm1.getText());
        pst.setString(3,txt_sm_abo1.getText());
        pst.setString(4,txt_sm_abohago1.getText());
        String a=txt_mefleyi_kutsri1.getText();
        String b=txt_sm1.getText();
        String c=txt_sm_abo1.getText();
        String d=txt_sm_abohago1.getText();
        rs=pst.executeQuery();
        if(rs.next())
        {
        try{
        String sql="update criminal set id=? and fname=? and lname=? and gf_name=? and wereda=? and kebelie=? and kushet=? and age=?and gender=? and education_level=?"
                + "and citizenship=? and religion=? and work_status=? and phone_number=? and date_and_time=? and crime_type=? and crime_reason=? and complaint_id=?"
                + " where id=? and fname=? and lname=? and gf_name=? ";  
        pst=conn.prepareStatement(sql);
        pst.setString(1,txt_mefleyi_kutsri.getText());
        pst.setString(2,txt_sm.getText());
        pst.setString(3,txt_sm_abo.getText());
        pst.setString(4,txt_sm_abohago.getText());
        pst.setString(5,txt_wereda.getText());
        pst.setString(6,txt_kebelie.getText());
        pst.setString(7,txt_kushet.getText());
        pst.setString(8,txt_edme.getText());
         pst.setString(9,txt_tsota.getText());
        pst.setString(10,txt_dereja_tmhrti.getText());
        pst.setString(11,txt_ziegnet.getText());
        pst.setString(12,txt_haymanot.getText());
        pst.setString(13,txt_srah_kunetat.getText());
        pst.setString(14,txt_kutsri_slki.getText());
        //pst.setString(15,((JTextField)txt_elet.getDateEditor().getUiComponent()).getText());
        pst.setString(16,txt_aynet_geben.getText());
        pst.setString(17,txt_mknyat_geben.getText());
        pst.setString(18,txt_kesasi_mefleyi_kutsri.getText());
        pst.setString(19,a);
        pst.setString(20,b);
        pst.setString(21,c);
        pst.setString(22,d);
        pst.execute();
      
      JOptionPane.showMessageDialog(null,"updated sucessfully");
      
         
      }
      catch(Exception e)
      {
       JOptionPane.showMessageDialog(null,e);
      }
     }
        else{
             JOptionPane.showMessageDialog(null,"there is no video with this title");   
           }
     }
      catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
}     
    }//GEN-LAST:event_jButton2ActionPerformed

    private void txt_mefleyi_kutsriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mefleyi_kutsriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_mefleyi_kutsriActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
       try{
        String s="select * from criminal where id=? and fname=? and lname=? and gf_name=? ";  
        pst=conn.prepareStatement(s);
        pst.setString(1,txt_mefleyi_kutsri1.getText());
        pst.setString(2,txt_sm1.getText());
        pst.setString(3,txt_sm_abo1.getText());
        pst.setString(4,txt_sm_abohago1.getText());
        rs=pst.executeQuery();
        if(rs.next())
        {
            String add1=rs.getString("fname");
            txt_sm.setText(add1);
            String add2=rs.getString("lname");
            txt_sm_abo.setText(add2);
            String add3=rs.getString("gf_name");
            txt_sm_abohago.setText(add3);
            String add4=rs.getString("wereda");
            txt_wereda.setText(add4);
            String add5=rs.getString("kebelie");
            txt_kebelie.setText(add5);
            String add6=rs.getString("kushet");
            txt_kushet.setText(add6);
            String add7=rs.getString("age");
            txt_edme.setText(add7);
            String add8=rs.getString("gender");
            txt_tsota.setText(add8);
            String add9=rs.getString("education_level");
            txt_dereja_tmhrti.setText(add9);
            String add110=rs.getString("citizenship");
            txt_haymanot.setText(add110);
            String add111=rs.getString("religion");
            txt_haymanot.setText(add111);
            String add12=rs.getString("work_status");
            txt_srah_kunetat.setText(add12);
            String add13=rs.getString("phone_number");
            txt_kutsri_slki.setText(add13);
            Date add14=rs.getDate("date_and_time");
            //txt_elet.setDate(add14);
            String add15=rs.getString("crime_type");
            txt_aynet_geben.setText(add15);
            String add16=rs.getString("crime_reason");
            txt_mknyat_geben.setText(add16);
            String add17=rs.getString("complaint_id");
            txt_kesasi_mefleyi_kutsri.setText(add17);
            String add18=rs.getString("year");
            txt_ametemhret.setText(add18);
        }  
        }
        catch(Exception e){
    JOptionPane.showMessageDialog(null,e);
}     
    }//GEN-LAST:event_jButton4ActionPerformed

    private void txt_weredaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_weredaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_weredaActionPerformed

    private void txt_kebelieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kebelieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kebelieActionPerformed

    private void txt_kushetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kushetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kushetActionPerformed

    private void txt_tsotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_tsotaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_tsotaActionPerformed

    private void txt_dereja_tmhrtiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_dereja_tmhrtiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_dereja_tmhrtiActionPerformed

    private void txt_ziegnetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ziegnetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_ziegnetActionPerformed

    private void txt_haymanotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_haymanotActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_haymanotActionPerformed

    private void txt_srah_kunetatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_srah_kunetatActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_srah_kunetatActionPerformed

    private void txt_kutsri_slkiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kutsri_slkiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kutsri_slkiActionPerformed

    private void txt_mknyat_gebenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mknyat_gebenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_mknyat_gebenActionPerformed

    private void txt_kesasi_mefleyi_kutsriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kesasi_mefleyi_kutsriActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kesasi_mefleyi_kutsriActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        dispose();
        new loginframe().setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void txt_printActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_printActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_printActionPerformed

    private void txt_ametemhretActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ametemhretActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_ametemhretActionPerformed

    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new criminal_detail().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton9;
    private com.toedter.calendar.JDateChooser jDateChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txt_ametemhret;
    private javax.swing.JTextField txt_aynet_geben;
    private javax.swing.JTextField txt_dereja_tmhrti;
    private javax.swing.JTextField txt_edme;
    private javax.swing.JTextField txt_elet1;
    private javax.swing.JTextField txt_haymanot;
    private javax.swing.JTextField txt_kebelie;
    private javax.swing.JTextField txt_kesasi_mefleyi_kutsri;
    private javax.swing.JTextField txt_kushet;
    private javax.swing.JTextField txt_kutsri_slki;
    private javax.swing.JTextField txt_mefleyi_kutsri;
    private javax.swing.JTextField txt_mefleyi_kutsri1;
    private javax.swing.JTextField txt_mknyat_geben;
    private javax.swing.JButton txt_print;
    private javax.swing.JTextField txt_sm;
    private javax.swing.JTextField txt_sm1;
    private javax.swing.JTextField txt_sm_abo;
    private javax.swing.JTextField txt_sm_abo1;
    private javax.swing.JTextField txt_sm_abohago;
    private javax.swing.JTextField txt_sm_abohago1;
    private javax.swing.JTextField txt_srah_kunetat;
    private javax.swing.JTextField txt_tsota;
    private javax.swing.JTextField txt_wereda;
    private javax.swing.JTextField txt_ziegnet;
    // End of variables declaration//GEN-END:variables
}
