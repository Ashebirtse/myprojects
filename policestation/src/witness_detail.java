import java.awt.*;
import java.awt.event.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.text.SimpleDateFormat;
import javax.swing.*;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import net.proteanit.sql.DbUtils;
import sun.audio.*;
import sun.audio.AudioStream;
public class witness_detail extends javax.swing.JFrame {
    Connection conn=null;
    ResultSet rs=null;
    PreparedStatement pst=null;
    public witness_detail() {
        initComponents();
        conn=connectionclass.myconnection();
        setTitle("Police Station Management System");
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt_sm = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_wereda = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_kebelie = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txt_sm_abo = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_sm_abohago = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txt_edme = new javax.swing.JTextField();
        txt_kushet = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_tsota = new javax.swing.JTextField();
        txt_print = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jButton4 = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        txt_gebenegna_mefleyi_kutsri1 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_sm1 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_sm_abo1 = new javax.swing.JTextField();
        txt_sm_abohago1 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txt_gebenegna_mefleyi_kutsri = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txt_kesasi_mefleyi_kutsri = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txt_kal_meskari = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_kesasi_mefleyi_kutsri1 = new javax.swing.JTextField();
        jButton9 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txt_sm.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_sm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_smActionPerformed(evt);
            }
        });

        jLabel18.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel18.setText("ወረዳ");

        txt_wereda.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_wereda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_weredaActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel19.setText("ቀበሌ");

        jLabel3.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel3.setText("ሽም");

        txt_kebelie.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kebelie.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kebelieActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel4.setText("ሽም ኣቦ");

        jLabel20.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel20.setText("ቁሸት");

        txt_sm_abo.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Nyala", 0, 11)); // NOI18N
        jLabel5.setText("ሽም ኣቦሓጎ");

        txt_sm_abohago.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_sm_abohago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_sm_abohagoActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel6.setText("ዕድመ");

        txt_edme.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_edme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_edmeActionPerformed(evt);
            }
        });

        txt_kushet.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_kushet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_kushetActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel21.setText("ፆታ");

        txt_tsota.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_tsota.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_tsotaActionPerformed(evt);
            }
        });

        txt_print.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        txt_print.setText("ፕሪንት");
        txt_print.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_printActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton1.setText("መዝግብ");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel14.setText("ስም ኣቦሓጎ");

        jButton4.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton4.setText("ድለይ");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Nyala", 0, 18)); // NOI18N
        jLabel16.setText("ከስተኻኽልዎ ናይ ዝደልዩ መስካሪ ቅጥዒ ይምልኡ!");

        jLabel9.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel9.setText("ገበነኛ መፍለዪ ቁፅሪ");

        txt_gebenegna_mefleyi_kutsri1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel12.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel12.setText("ስም");

        txt_sm1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel13.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel13.setText("ስም ኣቦ");

        txt_sm_abo1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        txt_sm_abohago1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jButton2.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton2.setText("ኣስተኻኽል");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton3.setText("ደምስስ");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Nyala", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 255));
        jLabel1.setText("ናይ መስከርቲ መመዝገቢ ቅጥዒ ");

        txt_gebenegna_mefleyi_kutsri.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel17.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel17.setText("ገበነኛ መፍለዪ ቁፅሪ");

        jLabel22.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel22.setText("ከሳሲ መፍለዪ ቁፅሪ");

        txt_kesasi_mefleyi_kutsri.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel15.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel15.setText("ቃል መስካሪ");

        txt_kal_meskari.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jLabel10.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jLabel10.setText("ከሳሲ መፍለዪ ቁፅሪ");

        txt_kesasi_mefleyi_kutsri1.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N

        jButton9.setFont(new java.awt.Font("Nyala", 0, 14)); // NOI18N
        jButton9.setText("ውፃእ");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(178, 178, 178)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 535, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(174, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(45, 45, 45)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_wereda)
                                    .addComponent(txt_sm_abohago)
                                    .addComponent(txt_sm_abo)
                                    .addComponent(txt_sm, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_kebelie, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(32, 32, 32)
                                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(6, 6, 6))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txt_kushet, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_edme, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_tsota, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_kal_meskari, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_print, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_gebenegna_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_kesasi_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 397, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(40, 40, 40)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txt_sm_abohago1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(8, 8, 8)
                                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(txt_sm1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(3, 3, 3)
                                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(txt_sm_abo1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(35, 35, 35)))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel9)
                                    .addGap(18, 18, 18)
                                    .addComponent(txt_gebenegna_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txt_kesasi_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton9)
                        .addGap(160, 160, 160))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_gebenegna_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kesasi_mefleyi_kutsri1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm_abo1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm_abohago1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton9, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_gebenegna_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kesasi_mefleyi_kutsri, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txt_sm, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_sm_abo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_sm_abohago, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_wereda, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kebelie, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kushet, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_edme, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_tsota, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_kal_meskari, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_print)
                            .addComponent(jButton1))))
                .addGap(196, 196, 196))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_smActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_smActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_smActionPerformed

    private void txt_weredaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_weredaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_weredaActionPerformed

    private void txt_kebelieActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kebelieActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kebelieActionPerformed

    private void txt_sm_abohagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_sm_abohagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_sm_abohagoActionPerformed

    private void txt_edmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_edmeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_edmeActionPerformed

    private void txt_kushetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_kushetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_kushetActionPerformed

    private void txt_tsotaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_tsotaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_tsotaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        try{
            String sql="insert into mskrat(gebenegna_id,kesasi_id,sm,sm_abo,sm_abohago,wereda,kebelie,kushet,edme,tsota,kal_meskari)"
                    + "values(?,?,?,?,?,?,?,?,?,?,?) ";
            pst=conn.prepareStatement(sql);
            pst.setString(1,txt_gebenegna_mefleyi_kutsri.getText());
            pst.setString(2,txt_kesasi_mefleyi_kutsri.getText());
            pst.setString(3,txt_sm.getText());
            pst.setString(4,txt_sm_abo.getText());
            pst.setString(5,txt_sm_abohago.getText());
            pst.setString(6,txt_wereda.getText());
            pst.setString(7,txt_kebelie.getText());
            pst.setString(8,txt_kushet.getText());
            pst.setString(9,txt_edme.getText());
            pst.setString(10,txt_tsota.getText());
            pst.setString(11,txt_kal_meskari.getText());
            pst.execute();

            JOptionPane.showMessageDialog(null,"registered sucessfully");

            // updatejtable();
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null,e);

        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        try{
            String s="select * from mskrat where gebenegna_id=? and kesasi_id=? and sm=? and sm_abo=? and sm_abohago=? ";
            pst=conn.prepareStatement(s);
            pst.setString(1,txt_gebenegna_mefleyi_kutsri1.getText());
            pst.setString(2,txt_kesasi_mefleyi_kutsri1.getText());
            pst.setString(3,txt_sm1.getText());
            pst.setString(4,txt_sm_abo1.getText());
            pst.setString(5,txt_sm_abohago1.getText());
            rs=pst.executeQuery();
            if(rs.next())
            {
                String add1=rs.getString("gebenegna_id");
                txt_gebenegna_mefleyi_kutsri.setText(add1);
                String add2=rs.getString("kesasi_id");
                txt_kesasi_mefleyi_kutsri.setText(add2);
                String add3=rs.getString("sm");
                txt_sm.setText(add3);
                String add4=rs.getString("sm_abo");
                txt_sm_abo.setText(add4);
                String add5=rs.getString("sm_abohago");
                txt_sm_abohago.setText(add5);
                String add6=rs.getString("wereda");
                txt_wereda.setText(add6);
                String add7=rs.getString("kebelie");
                txt_kebelie.setText(add7);
                String add8=rs.getString("kushet");
                txt_kushet.setText(add8);
                String add9=rs.getString("edme");
                txt_edme.setText(add9);
                String add10=rs.getString("tsota");
                txt_tsota.setText(add10);
                String add11=rs.getString("kal_meskari");
                txt_kal_meskari.setText(add11);


            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try{
            String s="select * from mskrat where gebenegna_id=? and kesasi_id=? and sm=? and sm_abo=? and sm_abohago=? ";
            pst=conn.prepareStatement(s);
            pst.setString(1,txt_gebenegna_mefleyi_kutsri1.getText());
            pst.setString(2,txt_kesasi_mefleyi_kutsri1.getText());
            pst.setString(3,txt_sm1.getText());
            pst.setString(4,txt_sm_abo1.getText());
            pst.setString(5,txt_sm_abohago1.getText());
            rs=pst.executeQuery();
            String a=txt_gebenegna_mefleyi_kutsri1.getText() ;
            String b=txt_kesasi_mefleyi_kutsri1.getText();
            if(rs.next())
            {
                try{
                    String sql="update mskrat set gebenegna_id=?, kesasi_id=?, sm=?,sm_abo=?, sm_abohago=?,"
                            + " wereda=?,kebelie=?,kushet=?,edme=?,tsota=?,kal_meskari=?"
                            + " where gebenegna_id=? and kesasi_id=?";
                    pst=conn.prepareStatement(sql);
                    pst.setString(1,txt_gebenegna_mefleyi_kutsri.getText());
                    pst.setString(2,txt_kesasi_mefleyi_kutsri.getText());
                    pst.setString(3,txt_sm.getText());
                    pst.setString(4,txt_sm_abo.getText());
                    pst.setString(5,txt_sm_abohago.getText());
                    pst.setString(6,txt_wereda.getText());
                    pst.setString(7,txt_kebelie.getText());
                    pst.setString(8,txt_kushet.getText());
                    pst.setString(9,txt_edme.getText());
                    pst.setString(10,txt_tsota.getText());
                    pst.setString(11,txt_kal_meskari.getText());
                    pst.setString(12,a);
                    pst.setString(13,b);
                    pst.execute();

                    JOptionPane.showMessageDialog(null,"updated sucessfully");

                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null,e);
                }
            }
            else{
                JOptionPane.showMessageDialog(null,"there is no witness with those specification");
            }
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try{
            String s="select * from mskrat where gebenegna_id=? and kesasi_id=? and sm=? and sm_abo=? and sm_abohago=? ";
            pst=conn.prepareStatement(s);
            pst.setString(1,txt_gebenegna_mefleyi_kutsri1.getText());
            pst.setString(2,txt_kesasi_mefleyi_kutsri1.getText());
            pst.setString(3,txt_sm1.getText());
            pst.setString(4,txt_sm_abo1.getText());
            pst.setString(5,txt_sm_abohago1.getText());
            rs=pst.executeQuery();
            if(rs.next())
            {
                try{
                    String sql="delete from mskrat where gebenegna_id=? and kesasi_id=? ";
                    pst=conn.prepareStatement(sql);
                    pst.setString(1,txt_gebenegna_mefleyi_kutsri1.getText());
                    pst.setString(2,txt_kesasi_mefleyi_kutsri1.getText());
                    pst.execute();

                    JOptionPane.showMessageDialog(null,"deleted sucessfully");

                }
                catch(Exception e)
                {
                    JOptionPane.showMessageDialog(null,e);
                }
            }
            else
            JOptionPane.showMessageDialog(null,"there is no witness with those specifications");
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null,e);
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        dispose();
        new loginframe().setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void txt_printActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_printActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_printActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(witness_detail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(witness_detail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(witness_detail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(witness_detail.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new witness_detail().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txt_edme;
    private javax.swing.JTextField txt_gebenegna_mefleyi_kutsri;
    private javax.swing.JTextField txt_gebenegna_mefleyi_kutsri1;
    private javax.swing.JTextField txt_kal_meskari;
    private javax.swing.JTextField txt_kebelie;
    private javax.swing.JTextField txt_kesasi_mefleyi_kutsri;
    private javax.swing.JTextField txt_kesasi_mefleyi_kutsri1;
    private javax.swing.JTextField txt_kushet;
    private javax.swing.JButton txt_print;
    private javax.swing.JTextField txt_sm;
    private javax.swing.JTextField txt_sm1;
    private javax.swing.JTextField txt_sm_abo;
    private javax.swing.JTextField txt_sm_abo1;
    private javax.swing.JTextField txt_sm_abohago;
    private javax.swing.JTextField txt_sm_abohago1;
    private javax.swing.JTextField txt_tsota;
    private javax.swing.JTextField txt_wereda;
    // End of variables declaration//GEN-END:variables
}
